**MODBUS/TCP**<br>
This project cover the client and server sides. <br>

**Server**<br>
Modbus.inc is a module written in Object Pascal that recevice and elaborate <br>
incoming modbus request and responding only to function code 0x03 or 0x10.<br>
Variables `parte_frontale` and `parte_logica` are pointer to physical structures <br>
on the RTU (remote terminal unit) that rappresent registers of the modbus protocol <br> 
<br>
**Client**<br>
Into the directory `client_modbus-tcp` there're all files necessary to build <br>
the client. <br>
This client was created with Lazarus version 2.0.2 and fpc 3.0.4. <br>
The project need `synapse` and `lnet` packages, that can be found in the online <br>
package manager of Lazarus (together with relatives visuals packs).<br>
In addition to that is needed BGRAControls package for buttons and progress bar.<br>
<br>
**Report**<br>
It describe briefly the basics of Modbus TCP and how to proceed with project structure.<br>
