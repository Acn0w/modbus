//Author: Alessandro Cecchin AKA Acn0w
const
 LAN_CAPTION = 'Language Preference';
 LAN_MSG = 'Which language do you prefer?' + sLineBreak + 'Quale lingua preferisci?';
 EN = 'English';
 IT = 'Italiano';

resourcestring
  PCK_SENT = 'Packet Sent';
  PCK_REC = 'Packet Received';
  REG = 'reg ';
  MOD_READ = 'Reading';
  MOD_WRITE = 'Writing';
  RADIO_TITLE = 'Mode';
  IP_TITLE = 'IP Address';
  PORT = 'Port';
  CON_BUT = 'CONNECT';
  SEND_BUT = 'SEND';
  REG_START = 'Start Register';
  QUA_REG = 'Number of Registers';
  REG_READ = 'Registers read';
  REG_WRITE = 'Registers wrote';
  FORM_CONN = 'Modbus TCP connected to ';
  FORM_DISC = 'Modbus TCP disconnected....';
  WAR_READ = '                   WARNING!!'+ LineEnding + 'Error Reading Holding Registers: ILLEGAL DATA ADDRESS';
  WAR_WRITE = '                   WARNING!!'+ sLineBreak+ 'Error Write Multiple Registers: ILLEGAL DATA ADDRESS';
  WAR_MISS_WRITE = '                   WARNING!!' + sLineBreak + 'Missing value in Writing fields';
  ERR_SENT = 'Error sending request';
  CON_LOST = 'Connection lost....';
  COM_MSG = 'Completed!';
  SEND_MSG = 'Sending....';
