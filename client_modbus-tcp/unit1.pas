//Author: Alessandro Cecchin AKA Acn0w
unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, DBCtrls,
  Buttons, ExtCtrls, Grids, lNetComponents, lNet, BGRAFlashProgressBar, BCMaterialDesignButton,
  SynaUtil, DefaultTranslator, LCLTranslator, ComCtrls, Spin, strutils;



{$INCLUDE captions.inc}

type

  { TForm1 }

  TForm1 = class(TForm)
    ConButt: TBCMaterialDesignButton;
    Label9: TLabel;
    SendButt: TBCMaterialDesignButton;
    FProgBar1: TBGRAFlashProgressBar;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Memo1: TMemo;
    Memo2: TMemo;
    RemoteSocket: TLTCPComponent;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    SpinEdit3: TSpinEdit;
    SpinEdit4: TSpinEdit;
    SpinEdit5: TSpinEdit;
    SpinEdit6: TSpinEdit;
    SpinEdit7: TSpinEdit;
    SpinEdit8: TSpinEdit;
    SpinEdit9: TSpinEdit;
    StringGrid1: TStringGrid;
    StringGrid2: TStringGrid;
    Timer1: TTimer;
    Timer2: TTimer;
    Timer3: TTimer;
    Timer4: TTimer;



    procedure ConButtClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RemoteSocketConnect(aSocket: TLSocket);
    procedure RemoteSocketDisconnect(aSocket: TLSocket);
    procedure RemoteSocketError(const msg: string; aSocket: TLSocket);
    procedure RemoteSocketReceive(aSocket: TLSocket);
    procedure SendButtClick(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure SpinEdit2Change(Sender: TObject);
    procedure SpinEdit3Change(Sender: TObject);
    procedure SpinEdit4Change(Sender: TObject);
    procedure StringGrid1KeyPress(Sender: TObject; var Key: char);
    procedure StringGrid1SelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Timer3Timer(Sender: TObject);
    procedure Timer4Timer(Sender: TObject);

  private
       sock : TLConnection;
       ID,TRAN_id : word;
       timeout : byte;
       queue_send: boolean;
       procedure readHoldingRegisters();
       procedure writeMultipleRegisters();
       procedure Terminator();

  public

  end;

 const
  MD_PROTO = 0;
  REC_LEN = 6;


var
  Form1: TForm1;


implementation

{$R *.lfm}

{ TForm1 }


//Initialize socket at form creation
procedure TForm1.FormCreate(Sender: TObject);
  begin

  (* //Create a popup dialog windows for selecting languages
  case QuestionDlg (LAN_CAPTION , LAN_MSG ,mtCustom,[mrYes,EN, mrNo, IT, 'IsDefault'],'') of

        mrYes:
            SetDefaultLang('en');
        mrNo:
            SetDefaultLang('it');
        mrCancel:
            Halt();
  end;*)
   //It gets the language direclty from OS preferences
    GetDefaultLang();

  //Initialize all based on the language detected
     ID := 0;
     sock := RemoteSocket;
     SendButt.Enabled := False;
     Memo1.Caption := '';
     Memo2.Caption := '';
     StringGrid1.InsertRowWithValues(0, [REG + '1', '0']);
     StringGrid1.RowCount := 1;
     Label18.Visible := True;
     Label18.Caption := '.   .   .   .';
     queue_send := False;

  //initialize all captions
     Label1.Caption := IP_TITLE;
     Label2.Caption := PORT;
     ConButt.Caption := CON_BUT;
     SendButt.Caption := SEND_BUT;
     Label16.Caption := MOD_READ;
     Label17.Caption := MOD_WRITE;
     Label8.Caption := REG_START;
     Label9.Caption := QUA_REG;
     Label10.Caption := REG_START;
     Label11.Caption := QUA_REG;
     Label12.Caption := REG_WRITE;
     Label13.Caption := REG_READ;
     Label6.Caption :=  PCK_SENT;
     Label7.Caption  := PCK_REC;


     //Activate debug mode so the interface can display received and sent packages
     if ParamCount > 1 then
       begin
         writeln('Unknown option arguments');
         writeln('More info with: " -h" or "-help"');
         writeln('');
         Halt;
       end;

     if (ParamCount = 1) then
     begin
          if ((ParamStr(1) = '-h') OR (ParamStr(1) = '-help') OR (ParamStr(1) = '--h') OR (ParamStr(1) = '--help')) then
            begin
                 Writeln('--dev or -d  -->  activate debuging mode with Packet Sent and Packet Received');
                 writeln('');
                 Halt;
            end;

          if ((ParamStr(1) = '-d') OR (ParamStr(1) = '-dev') OR (ParamStr(1) = '--d') OR (ParamStr(1) = '--dev')) then
             begin
              Memo1.Visible := True;
              Memo2.Visible := True;
              Label6.Visible := True;
              Label7.Visible := True;

             end
          else
          begin
               writeln('Unknown option arguments');
               writeln('More info with: " -h" or "-help"');
               writeln('');
               Halt;
          end;
     end;
end;


//CLICK OF THE LABEL CONNECT
procedure TForm1.ConButtClick(Sender: TObject);
  var
      address : string;
  begin
      //It compose the IP address
         address := IntToStr(SpinEdit6.Value) + '.' + IntToStr(SpinEdit7.Value) + '.' + IntToStr(SpinEdit8.Value) + '.' + IntToStr(SpinEdit9.Value);

         //Check if the Connect button is pressed (active) or not and change the
         //status of interface accordingly
         if (ConButt.NormalColor = clRed ) then
            begin
                 timeout := 0;
                 SpinEdit5.Enabled := False;
                 SpinEdit6.Enabled := False;
                 SpinEdit7.Enabled := False;
                 SpinEdit8.Enabled := False;
                 SpinEdit9.Enabled := False;
                 sock.Connect(address, Word(SpinEdit5.Value));
            end
         else
            begin
                 (*
                 Otherwise deactivate all.
                 Disconnect is forced with True value so Win7 doesn't throw error message.
                 (Windows 7 connection error 10054).
                 This error is fixed on lastest version of Windows.
                 *)
                 Timer1.Enabled := False;
                 sock.Disconnect(True);

                 Terminator();
            end;
end;



//CLICK OF THE LABEL SEND
procedure TForm1.SendButtClick(Sender: TObject);
begin
       (*First it has to stop reading, send the message and when the confirmation message arrive it restart reading.
       At every read request, it checks if the packet arrive, and then it send another read request.
       If it fails receiving, after a certain ammount of times, it considers the connection close.
       The same thing applies to send request.

       It set a flag to stop reading. Reading request are sequentials. If the flag is set and after receiving
       the last reading confirmation, it can send a write request. In addition it disable the Send label
       so the user can't submit more than one request. After the writing confirmation it re-activate the labels.  *)


    //set the reading flag
     queue_send := True;
     Label18.Caption := SEND_MSG;
     //disable the Send button
     SendButt.Enabled := False;
     ShowMessage (CON_LOST);

     //lock the send fields for more robust data integrity and consistency
     SpinEdit3.Enabled := False;
     SpinEdit4.Enabled := False;
     StringGrid1.Options := StringGrid1.Options - [goEditing];
end;


//if it receive an 'Illegal data address' at the first try, it restart everything
procedure TForm1.SpinEdit1Change(Sender: TObject);
begin
    Timer1.Enabled := True;
  end;


procedure TForm1.SpinEdit2Change(Sender: TObject);
  begin
    Timer1.Enabled := True;
  end;


(*Initialize the grid where user can write values.
At every edit of initial register (Writing), it create a new grid with one row
and the starting register given by the user. Following registers will be modified
when 'Number of Registers' changes.*)
procedure TForm1.SpinEdit3Change(Sender: TObject);
 var
   i :integer;

begin
     StringGrid1.Clear;
    try
     StringGrid1.RowCount := 0;
     for i :=1 to SpinEdit4.Value do
      begin
      //Create as many empty rows as 'Number of Registers' (in Writing) specifies
         StringGrid1.InsertRowWithValues(i - 1, [REG + IntToStr(SpinEdit3.Value + i -1), ' ']);

      end;
    except
      //When there's nothing in register field don't write anything in the grid
            on e: Exception do
            StringGrid1.RowCount := 0;

    end;
end;

procedure TForm1.SpinEdit4Change(Sender: TObject);
 var
   i : integer;
 begin
   try
     StringGrid1.RowCount := 0;

     for i :=1 to SpinEdit4.Value do
     begin
     //Insert as many lines as specified in 'Number of Registers' (Writing)
        StringGrid1.InsertRowWithValues(i - 1, [REG + IntToStr(SpinEdit3.Value + i -1), ' ']);
     end;

   except
     //When there's nothing in register field don't write anything in the grid
    on e: Exception do
       StringGrid1.RowCount := 0;

   end;
end;


//Check on input if in the grid there're oly numbers
procedure TForm1.StringGrid1KeyPress(Sender: TObject; var Key: char);
begin
    if not (Key in ['0'..'9', #8, #9]) then Key := #0;
end;


procedure TForm1.RemoteSocketConnect(aSocket: TLSocket);
begin
  //When it connects to the socket, it activate all labels and buttons
  ConButt.NormalColor := clLime;
  SendButt.NormalColor := clGradientActiveCaption;
  SendButt.Enabled := True;
  SpinEdit1.Enabled := True;
  SpinEdit2.Enabled := True;
  Label8.Enabled := True;
  Label9.Enabled := True;
  Label13.Enabled := True;
  SpinEdit3.Enabled := True;
  SpinEdit4.Enabled := True;
  Label10.Enabled := True;
  Label11.Enabled := True;
  Memo1.Lines.Add('Server connected...');
  Form1.Caption := FORM_CONN + IntToStr(SpinEdit6.Value) + '.' + IntToStr(SpinEdit7.Value) + '.' + IntToStr(SpinEdit8.Value) + '.' + IntToStr(SpinEdit9.Value) + '....';
  Timer1.Enabled := True;
end;


//If it disconnect fromt the server, disable all
procedure TForm1.RemoteSocketDisconnect(aSocket: TLSocket);
begin
     Timer1.Enabled := False;
     Timer2.Enabled := False;
     Timer3.Enabled := False;
     Timer4.Enabled := False;
     Terminator();

end;

//If the socket encounter an error, disable all
procedure TForm1.RemoteSocketError(const msg: string; aSocket: TLSocket);
begin
  Timer1.Enabled := False;
  Timer2.Enabled := False;
  Timer3.Enabled := False;
  Timer4.Enabled := False;
  ShowMessage(msg);
  Terminator();

end;

//PACKET RECEIVED
procedure TForm1.RemoteSocketReceive(aSocket: TLSocket);
var
  s : string;
  i,n : integer;
begin
   if aSocket.GetMessage(s) > 0 then
    begin
    Memo2.Lines.Add(StrToHex(s));            //paste on the second box

    //Extract the transaction id of the packet
    TRAN_id := ord(s[1])*256 + ord(s[2]);
    //Check the function code of arriving packet
    case ord(s[8]) of
         3:
             begin
                 FProgBar1.Value := 0;
                 if (TRAN_id = ID) then
                   begin
                      Timer2.Enabled := False;
                     //if the writing flag is set, then it stops reading
                     if (queue_send = True) then
                        begin
                           //Stop reading
                           Timer1.Enabled := False;
                           //Activate loading bar
                           Timer3.Enabled := True;
                           //Activate the timeout for the packet sent
                           timeout := 0;
                           Timer4.Enabled := True;
                           writeMultipleRegisters();
                        end
                     else
                       begin
                         //Otherwise keep reading
                         timeout := 0;
                         Timer1.Enabled := True;
                       end;
                    end;

                 //Extract data and paste into the grid
                 n := ord(s[9]) div 2;
                 StringGrid2.RowCount := n;
                 for i := 0 to (n-1) do
                     begin
                         StringGrid2.Cells[0,i] := REG + IntToStr(SpinEdit1.Value + i);
                         StringGrid2.Cells[1,i] := IntToStr(ord(s[10+i*2])*256 + ord(s[11+i*2]));
                     end;
             end;

         16:
             begin
                 if (ID = TRAN_ID) then
                  begin
                       timeout := 0;
                       queue_send := False;
                       //Stop the loading bar
                       Timer3.Enabled := False;
                       //Stop the timeout for write's packet
                       Timer4.Enabled := False;
                       FProgBar1.Value := 100;
                       Label18.Caption := COM_MSG;
                       //Restart the reading routine
                       Timer1.Enabled := True;
                       //Re-activate all buttons for a new writing request
                       SendButt.Enabled := True;
                       SpinEdit3.Enabled := True;
                       SpinEdit4.Enabled := True;
                       StringGrid1.Options := StringGrid1.Options + [goEditing];
                  end;
             end;
         131:
             begin
                  //Print out all MODBUS errors sent by the server and stop send reading requests
                 //until Reading's SpinEdit changes value
                  Timer1.Enabled := False;
                  Timer2.Enabled := False;
                  ShowMessage(WAR_READ);
                  timeout := 0;
             end;
         144:
             begin
                 //Stop all timers, re-activate Writing stuff
                  FprogBar1.Value := 0;
                  Label18.Caption := '. . . .';
                  Timer4.Enabled := False;
                  Timer3.Enabled := False;
                  ShowMessage(WAR_WRITE);
                  queue_send := False;
                  Timer1.Enabled := True;
                  SendButt.Enabled := True;
                  SpinEdit3.Enabled := True;
                  SpinEdit4.Enabled := True;
                  StringGrid1.Options := StringGrid1.Options + [goEditing];
                  timeout := 0;

             end;
    end;
   end;
end;


(*Since windows is a total mess and it can't follow some specific directives, I have
to add another constraint that change dynamically the editing capabilities on grid's
first column of writing register.
It detects which cell the cursor is above and change the property of cells accordingly*)
procedure TForm1.StringGrid1SelectCell(Sender: TObject; aCol, aRow: Integer;
  var CanSelect: Boolean);
begin
  if (queue_send = False ) then
   begin
          if ACol = 1 then
                StringGrid1.Options := StringGrid1.Options + [goEditing]
        else
                StringGrid1.Options := StringGrid1.Options - [goEditing];
   end;
end;


  //Every time 200ms expires, it send a reading requests
procedure TForm1.Timer1Timer(Sender: TObject);
begin
        Timer1.Enabled := False;
        //attivo il timeout per la lettura
        Timer2.Enabled := True;
        readHoldingRegisters();
end;

//Timer used as timeout for packets that I have still to receive. If it run out three times
//the connection is consider close.
procedure TForm1.Timer2Timer(Sender: TObject);
begin
          if (timeout = 2) then
           begin
                Timer1.Enabled := False;
                Timer2.Enabled := False;
                sock.Disconnect(True);
                Terminator();
                ShowMessage (CON_LOST);
                Exit();
           end;
          inc(timeout);
          Timer2.Enabled := False;
          Timer1.Enabled := True;
end;

//Timer used for progress/loading bar
procedure TForm1.Timer3Timer(Sender: TObject);
begin
      FProgBar1.Value := FProgBar1.Value + 1;
      if (FProgBar1.Value = 90) then
       Timer3.Enabled := False;
end;

//Timer for writing requests
procedure TForm1.Timer4Timer(Sender: TObject);
begin
    if (timeout = 2) then
       begin
        //Stop the bar and set to default
        Timer3.Enabled := False;
        FProgBar1.Value := 0;
        Label18.Caption := '. . . .';
        Timer4.Enabled := False;
        sock.Disconnect(True);
        ShowMessage(CON_LOST);
        Terminator();
        Exit();
      end;
     inc(timeout);
    //try to send again
    writeMultipleRegisters();
end;



//Generate strings in order to read on the remote server
procedure TForm1.readHoldingRegisters();
var
  tmpstr : string;
  tmpnum : integer;
begin
  //Increment transaction ID on every iteration
  if (ID = 65534) then
    ID := 0;

  inc(ID);
  tmpstr := '';
  tmpstr := chr(Byte(hi(ID))) + chr(Byte(lo(ID))) + chr(byte(hi(MD_PROTO))) + Chr(byte(lo(MD_PROTO)))
   + chr(byte(hi(REC_LEN))) + Chr(byte(lo(REC_LEN))) + #255 + #03 + Chr(byte(hi(SpinEdit1.Value)))
   + Chr(byte(lo(SpinEdit1.Value))) + Chr(byte(hi(SpinEdit2.Value))) + Chr(byte(lo(SpinEdit2.Value)));
  //debug print
  Memo1.Lines.Add(StrToHex(tmpstr));
  tmpnum := sock.SendMessage(tmpstr);
  if (tmpnum < Length(tmpstr)) then
      ShowMessage(ERR_SENT);

end;

//Generate strings in order to write on the remote server
procedure TForm1.writeMultipleRegisters();
  var
    tmpstr : string;
    i,tmpnum : integer;
  begin
    if (ID = 65534) then
      ID := 0;

    inc(ID);
    tmpstr := '';
    //Compose first part of the string
   try
       tmpstr := chr(Byte(hi(ID))) + chr(Byte(lo(ID))) + chr(byte(hi(MD_PROTO))) + Chr(byte(lo(MD_PROTO)))
       + chr(byte(hi(7+SpinEdit4.Value*2))) + Chr(byte(lo(7+SpinEdit4.Value*2))) + #255 + #16
       +Chr(byte(hi(SpinEdit3.Value))) + Chr(byte(lo(SpinEdit3.Value))) + Chr(byte(hi(SpinEdit4.Value)))
       + Chr(byte(lo(SpinEdit4.Value))) + chr(SpinEdit4.Value*2);

    //Add to the string the values readed in the input grid
    for i:=0 to (SpinEdit4.Value-1) do
      begin
          tmpstr := tmpstr + chr(byte(hi(StrToInt(StringGrid1.Cells[1,i])))) + Chr(byte(lo(StrToInt(StringGrid1.Cells[1,i]))));

      end;
      //debug print
      Memo1.Lines.Add(StrToHex(tmpstr));
      tmpnum := sock.SendMessage(tmpstr);
      if (tmpnum < Length(tmpstr)) then
        ShowMessage(ERR_SENT);
      except
      on e: Exception do
           begin

             //If there's an error lock the send's buttons and reactivate read's buttons
             SendButt.Enabled := True;
             SpinEdit3.Enabled := True;
             SpinEdit4.Enabled := True;
             StringGrid1.Options := StringGrid1.Options + [goEditing];
             Timer1.Enabled := True;
             Timer4.Enabled := False;
             Timer3.Enabled := False;
             FProgBar1.Value := 0;
             Label18.Caption := '.   .   .   .';
             queue_send := False;
             ShowMessage(WAR_MISS_WRITE);
           end;
      end;
  end;



procedure TForm1.Terminator();
  begin
   ConButt.NormalColor := clRed;
   SendButt.NormalColor := clSilver;
   SendButt.Enabled := False;
   SpinEdit1.Enabled := False;
   SpinEdit2.Enabled := False;
   Label10.Enabled := False;
   Label11.Enabled := False;
   Label8.Enabled := False;
   Label9.Enabled := False;
   SpinEdit3.Enabled := False;
   SpinEdit4.Enabled := False;
   Memo1.Lines.Add('Server disconnected...');
   Form1.Caption := (FORM_DISC);
   SpinEdit5.Enabled := True;
   SpinEdit6.Enabled := True;
   SpinEdit7.Enabled := True;
   SpinEdit8.Enabled := True;
   SpinEdit9.Enabled := True;
  end;

end.
