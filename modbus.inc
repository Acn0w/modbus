//Author: Alessandro Cecchin AKA Acn0w
type
    MBAP = record
      TRANS_ID : word;
      PROTO_ID : word; //MD_PROTO goes into it (constant)
      LENGHT : word;  //MODBUS PDU plus the Unit Identifier byte. This value is set in the 'Length' field.
      UNIT_ID : byte; //SLAVE_ID goes into it (constant)
    end;

type
  //PDU that will be concatenate to MBAP in order to obtain the final packet ready to be sent or recived
    MPDU = record
      FUN_CODE : byte;
      DATA : string;
    end;

//new class for MODBUS packet
TLTCP_modbus = class
  private
    FCon: TLTCP;
    procedure OnEr(const msg: string;aSocket: TLSocket);
    procedure OnAc(aSocket: TLSocket);
    procedure MB_OnRe(aSocket: TLSocket);
    procedure OnDs(aSocket: TLSocket);
  public
    constructor Create;
    destructor Destroy; override;
    procedure MB_Run; //it start the routine handling of the socket
    procedure CatchEvent; //catch all events of receiving packets
  end;


var
    sock : TLTCP_modbus;
    ptr_frontale, ptr_logica : ^integer;//they're needed in order to obtain data from the machine
                                        //connected to Gateway

const
    //MBAP parameters
    //since port 502 is reserved, and we don't have admin privileges, we use another arbitrary port
    MB_RPORT = 6667;
    SLAVE_ID = 255;
    MD_PROTO = 0;
    ERR_LEN = 3;


    //MODBUS errors
    ERR01 = 'IllegalFunctionCode: The function code is unknown by the server';
    ERR02 = 'IllegalDataAddress: Dependant on the request';
    ERR03 = 'IllegalDataValue: Dependant on the request';
    ERR04 = 'ServerFailure: The server failed during the execution';
    ERR05 = 'Acknowledge: The server accepted the service invocation, but the service requires a relatively long time to execute';
    ERR06 = 'ServerBusy: The server was unable to accept the MB Request PDU';
    ERR0A = 'GatewayProblem: Gateway paths not available';
    ERR0B = 'GatewayProblem: The targeted device failed to respond';


////////////////////////////////////////////////////////////////////////////////
////////////////////FUNCTION/CODE/ERRORS/HANDLING///////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//It create the error packet
function errorFC(header : MBAP;code : byte; error : byte) : string;

begin
  //error code = function code + 80
  code := code + 128;
  //composing string...
  Result := chr(Byte(hi(header.TRANS_ID))) + chr(Byte(lo(header.TRANS_ID))) + chr(byte(hi(MD_PROTO))) + Chr(byte(lo(MD_PROTO)))
   + chr(byte(hi(ERR_LEN))) + Chr(byte(lo(ERR_LEN))) + chr(Byte(SLAVE_ID)) + chr(code) + chr(error);
end;

//READ HOLDING REGISTERS (view MODBUS protocol)
function readHoldingRegisters(Identifier: word; data : MPDU):string;
var
  starting_addr,quanti_reg : word;
  tmpstr : string;
  count : integer;
begin
  count := 0;
  //it gain the starting registers and how many of them to read
  starting_addr := ord(data.DATA[1])*256 + ord(data.DATA[2]);
  quanti_reg := ord(data.DATA[3])*256 + ord(data.DATA[4]);

  //check range of registers
  if((starting_addr+quanti_reg) > 79) then
    begin
      //if not in range, send error string
      tmpstr := 'error';
      Exit(tmpstr);
    end;

  //It compose the response string as described by MODBUS protocol
  tmpstr :=  chr(Byte(hi(Identifier))) + chr(Byte(lo(Identifier))) + chr(byte(hi(MD_PROTO))) + Chr(byte(lo(MD_PROTO)))
   + chr(Byte(hi(2+quanti_reg*2))) + chr(Byte(lo(2+quanti_reg*2))) + chr(Byte(SLAVE_ID)) + #03 + Chr(byte(2*quanti_reg));


  for i := 0 to (quanti_reg-1) do
    begin

      if (starting_addr+i-1 < 54) then
        begin
          //add the value in the frontal register to the string
         tmpstr := tmpstr + chr(byte(hi(Word((ptr_frontale+starting_addr+i-1)^)))) + chr(byte(lo(Word((ptr_frontale+starting_addr+i-1)^))));
        end
      else
        //add the value in the logic register to the string
        begin
          tmpstr := tmpstr + chr(byte(hi(Word((ptr_logica+count)^)))) + chr(byte(lo(Word((ptr_logica+count)^))));
          inc(count);
        end;

    end;

  Result := tmpstr;

end;


////WRITE MULTIPLE REGISTERS (see MODBUS protocol)
function writeMultipleRegisters(Identifier: word;data : MPDU):string;
var
  starting_addr,quanti_reg : word;
  tmpstr : string;
  count : integer;
begin
  count := 0;
  starting_addr := ord(data.DATA[1])*256 + ord(data.DATA[2]);
  quanti_reg := ord(data.DATA[3])*256 + ord(data.DATA[4]);

  //check range of registers
  if((starting_addr+quanti_reg) > 79) then
    begin
      //if not in range, send error string
      tmpstr := 'error';
      Exit(tmpstr);
    end;

    for i := 0 to (quanti_reg-1) do
    begin

      if (starting_addr+i-1 < 54) then
        begin

      //It write the received value into frontal register
      //ord return a 16bit unsigned integer
      ptr_frontale[starting_addr+i-1] := ord(data.DATA[6+2*i])*256 + ord(data.DATA[7+2*i]);
      //It send received data to UART serial port in order to be stored inside RTU (remote terminal unit)
      NumMemImp := ord(data.DATA[6+2*i])*256 + ord(data.DATA[7+2*i]);
      //NOTE: this is a call to proprietary method on another program.
      //See serial UART protocolfor implementation
      InviaRisposta;

      end
      else
        ////It write the received value into logical register
        begin
          ptr_logica[count] := ord(data.DATA[6+2*i])*256 + ord(data.DATA[7+2*i]);
          inc(count);
        end;

    end;
    //Create ACK response string as protocol
    tmpstr := chr(byte(hi(Identifier))) + chr(byte(lo(Identifier))) + chr(byte(hi(MD_PROTO))) + chr(byte(lo(MD_PROTO)))
    + chr(byte(hi(6))) + chr(byte(lo(6))) + chr(byte(SLAVE_ID)) + #16 + chr(byte(hi(starting_addr))) +
    chr(byte(lo(starting_addr))) + chr(byte(hi(quanti_reg))) + chr(byte(lo(quanti_reg)));
  Result := tmpstr;
end;

////////////////////////////////////////////////////////////////////////////////
//////////////////////////CONSTRUCTOR/MODBUS////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//create socket for modbus transmission
constructor TLTCP_modbus.Create;
begin
  FCon := TLTCP.Create(nil); // create new TCP connection
  FCon.OnError := @OnEr;
  FCon.OnReceive := @MB_OnRe;
  FCon.OnDisconnect := @OnDs; //call disconnect method of synapse https://wiki.freepascal.org/Synapse
  FCon.OnAccept := @OnAc;
  FCon.Timeout := 20; // responsive enough, but won't hog cpu
  FCon.ReuseAddress := True;
end;
////////////////////////////////////////////////////////////////////////////////
///////////////////////SOCKET/ACTIONS///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//WriteFileLog is a external method that write out son info on a file (It's proprietary)
procedure TLTCP_modbus.OnAc(aSocket: TLSocket);
begin
  WriteLn('Accepted...');
  WriteFileLog(FormatDateTime('hh:nn:ss.zzz',Now)+' - TLTCP_modbus.OnAc: Connection accepted from '+ aSocket.PeerAddress);
end;

procedure TLTCP_modbus.OnDs(aSocket: TLSocket);
begin
  WriteLn('Disconnected...');
  WriteFileLog(FormatDateTime('hh:nn:ss.zzz',Now)+' - TLTCP_modbus.OnDs: Lost connection with' + aSocket.PeerAddress);
end;

//INITIALIZE/SOCKET,/MAKE/IT/LISTENING/AND/EXECUTE/CALLACTION
procedure TLTCP_modbus.MB_Run;
begin
    WriteLn('Listening...');
    //It tries to start the socket and listening on the arbitrary port choosen
    FCon.Listen(Word(MB_RPORT));

end;


procedure TLTCP_modbus.OnEr(const msg: string; aSocket: TLSocket);
begin
  WriteFileLog(FormatDateTime('hh:nn:ss.zzz',Now)+' - TLTCP_modbus.OnEr: Errore TCP -> '+ msg);
end;


procedure TLTCP_modbus.CatchEvent;
begin
  FCon.CallAction;
end;

destructor TLTCP_modbus.Destroy;
begin
  FCon.Free; // free the TCP connection -> delete socket object
  inherited Destroy;
end;



////////////////////////////////////////////////////////////////////////////////
////////////////////////PROCESS/ALL/INCOMING/PACKETS////////////////////////////
////////////////////////////////////////////////////////////////////////////////

procedure TLTCP_modbus.MB_OnRe(aSocket: TLSocket);
var
  s,TempStr,errorCheck: string;
  n: Integer;
  pcktHEADER : MBAP;
  pcktDATA : MPDU;
begin

  //if it recive something, it start to process packets
  if aSocket.GetMessage(s) > 0 then
    begin
    //write request packet one terminal
    WriteLn('Message received: ' + StrToHex(s));

    //it insert TRANS_ID into the structure: it read 2 bytes and insert it.
    pcktHEADER.PROTO_ID := ord(s[3])*256+ord(s[4]);

    //if the protocol isn't compatible with MODBUS, it exit this routine and wait for another packet.
    if pcktHEADER.PROTO_ID <> 0 then
      Exit;

    //It fill in the remaining valure
    pcktHEADER.TRANS_ID := ord(s[1])*256 + ord(s[2]);
    pcktHEADER.LENGHT := ord(s[5])*256 + ord(s[6]);
    pcktHEADER.UNIT_ID := SLAVE_ID;
    pcktDATA.FUN_CODE := ord(s[8]);
    pcktDATA.DATA := Copy(s,9,pcktHEADER.LENGHT-2);

    case (pcktDATA.FUN_CODE) of

      3 :
        begin
          errorCheck := readHoldingRegisters(pcktHEADER.TRANS_ID,pcktDATA);
          //when error is received it call errorFC
          if (errorCheck = 'error' ) then
            begin
              errorCheck := errorFC(pcktHEADER,3,2);
              n := FCon.SendMessage(errorCheck, aSocket);
              if (n < Length(errorCheck)) then // try to send
                WriteFileLog('TLTCPTest.MB_OnRe -> Unsuccessful send, wanted: '+ IntToStr(Length(errorCheck)) + ' got: '+ IntTostr(n)); // if send fails write error
              // if FUN_CODE not found print on log
              WriteFileLog('TLTCP_modbus.MB_OnRe -> ' + ERR02);

              Exit;
            end;
            //If all goes right, the string error check it send it
            WriteLn('Message sent: ' + StrToHex(errorCheck));
            n := FCon.SendMessage(errorCheck, aSocket);
            if (n < Length(errorCheck)) then // try to send
              WriteFileLog('TLTCPTest.MB_OnRe -> Unsuccessful send, wanted: '+ IntToStr(Length(errorCheck)) + ' got: '+ IntTostr(n)); // if send fails write error
            Exit;

        end;

      16 :
         begin
          errorCheck := writeMultipleRegisters(pcktHEADER.TRANS_ID,pcktDATA);
          //when error is received it call errorFC
          if (errorCheck = 'error' ) then
            begin
              errorCheck := errorFC(pcktHEADER,16,2);
              n := FCon.SendMessage(errorCheck, aSocket);
              if (n < Length(errorCheck)) then // try to send m
                WriteFileLog('TLTCPTest.MB_OnRe -> Unsuccessful send, wanted: '+ IntToStr(Length(errorCheck)) + ' got: '+ IntTostr(n)); // if send fails write error
              // if FUN_CODE not found
              WriteFileLog('TLTCP_modbus.MB_OnRe -> ' + ERR02);

              Exit;
            end;

            WriteLn('Message sent: ' + StrToHex(errorCheck));
            n := FCon.SendMessage(errorCheck, aSocket);
            if (n < Length(errorCheck)) then // try to send to each of them
              WriteFileLog('TLTCPTest.MB_OnRe -> Unsuccessful send, wanted: '+ IntToStr(Length(errorCheck)) + ' got: '+ IntTostr(n)); // if send fails write error
            Exit;
            end;

      else
        begin
          //if the function code isn't recognize, it send back an error code for the client
          TempStr := errorFC(pcktHEADER,pcktDATA.FUN_CODE,1);
          n := FCon.SendMessage(TempStr, aSocket);
          if (n < Length(TempStr)) then // try to send 
            WriteFileLog('TLTCPTest.MB_OnRe -> Unsuccessful send, wanted: '+ IntToStr(Length(TempStr)) + ' got: '+ IntTostr(n)); // if send fails write error
          // if FUN_CODE not found
          WriteFileLog('TLTCP_modbus.MB_OnRe -> ' + ERR01);

          Exit;
        end;
    end;
  end;
end;
